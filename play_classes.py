# -*- encoding:utf-8 -*-
class Character:
    def __init__(self, life_points, attack_points):
        self.life_points = life_points
        self.attack_points = attack_points

    def get_life_points(self):
        return self.life_points

    def set_life_points(self, new_life_points):
        self.life_points = new_life_points

    def get_attack_points(self):
        return self.attack_points

    def set_attack_points(self, new_attack_points):
        self.attack_points = new_attack_points


class Player(Character):

    def __init__(self, life_points, attack_points):
        super(Player, self).__init__(life_points, attack_points)
        self.is_hit = False
        self.is_dead = False

    def get_is_hit(self):
        return self.is_hit

    def set_is_hit(self, is_hit_value):
        self.is_hit = is_hit_value

    def get_is_dead(self):
        return self.is_dead

    def set_is_dead(self, is_dead_value):
        self.is_dead = is_dead_value

    def check_if_dead(self):
        life_points = self.get_life_points()
        if life_points <= 0:
            self.set_is_dead(True)
        elif life_points > 0:
            self.set_is_dead(False)

    def hited(self, attack_points):
        is_it_hited = self.get_is_hit()
        is_it_dead = self.get_is_dead()
        if is_it_dead == True:
            print("DEAD")
            exit()
        elif is_it_dead == False:
            if is_it_hited == True:
                self.life_points -= attack_points
            elif is_it_hited == False:
                pass


# Test phase
if __name__ == '__main__':
    player1 = Player(100, 10)
    player2 = Player(100, 20)
    lp1 = player1.get_life_points()
    print("LP 1: ", lp1)
    ap1 = player1.get_attack_points()
    print("AP 1: ", ap1)

    lp2 = player2.get_life_points()
    print("LP 2: ", lp2)
    ap2 = player2.get_attack_points()
    print("AP 2: ", ap2)

    print("Game Start")
    print("HITED")
    player1.set_is_hit(True)
    player1.hited(ap2)
    player1.check_if_dead()
    player1.hited(ap2)
    new_lp = player1.get_life_points()
    
    print("LP 1: ", new_lp)