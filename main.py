"""
Arcade Game
"""
import arcade
import os
from _variables import *
from player import Player


class Game(arcade.Window):
    def __init__(self):
        # Call the parent class and set up the window
        super().__init__(SCREEN_WIDTH, SCREEN_HEIGHT, SCREEN_TITLE)

        # Set the background color
        arcade.set_background_color(arcade.color.AMAZON)

        # Variables that will hold sprite lists
        self.floor_list = arcade.SpriteList()

        # Variable physique/gravité
        self.physics_engine = None

        # Set up du background
        self.background = arcade.load_texture("resources/background.gif")

        self.setup()

    def setup(self):
        # Set up du sol
        for x in range(0, 1250, 64):
            floor = arcade.Sprite("resources/grassMid.png", TILE_SCALING)
            floor.center_x = x
            floor.center_y = 32
            # Set sol transparent. 0 to 255
            floor.alpha = 0
            self.floor_list.append(floor)

        # Mise en place de la physique grâce à la gravité
        self.physics_engine = arcade.PhysicsEnginePlatformer(Player.walk_sprite, self.floor_list, GRAVITY)

        # Player.update(self)

    def on_draw(self):
        """ Render the screen. """
        arcade.start_render()
        # Dessine Background image
        arcade.draw_texture_rectangle(SCREEN_WIDTH // 2, SCREEN_HEIGHT // 2,
                                      SCREEN_WIDTH, SCREEN_HEIGHT, self.background)
        # Dessine les sprites
        self.floor_list.draw()


def main():
    window = Game()
    window.setup()
    player = Player()
    player.setup()
    arcade.run()


if __name__ == "__main__":
    main()
