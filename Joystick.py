import os, struct, array
from fcntl import ioctl


class Joystic:
    def __init__(self, input_file):
        # We'll store the states here.
        self.axis_states = {}
        self.button_states = {}

        # These constants were borrowed from linux/input.h
        self.axis_names = {
            0x00: 'x',
            0x01: 'y',
        }

        self.axis_map = []
        self.button_map = []
        self.jostick_dev = open(input_file, 'rb')

        self.button_names = {
            0x120: 'trigger',
            0x121: 'thumb',
            0x122: 'thumb2',
            0x123: 'top',
            0x124: 'top2',
            0x125: 'pinkie',
            0x126: 'base',
            0x127: 'base2',
            0x128: 'base3',
            0x129: 'base4',
            0x12a: 'base5',
            0x12b: 'base6'
        }
        # Get number of axes and buttons.
        buf = array.array('B', [0])
        ioctl(self.jostick_dev, 0x80016a11, buf)  # JSIOCGAXES
        num_axes = buf[0]

        buf = array.array('B', [0])
        ioctl(self.jostick_dev, 0x80016a12, buf)  # JSIOCGBUTTONS
        num_buttons = buf[0]

        # Get the axis map.
        buf = array.array('B', [0] * 0x40)
        ioctl(self.jostick_dev, 0x80406a32, buf)  # JSIOCGAXMAP

        for axis in buf[:num_axes]:
            axis_name = self.axis_names.get(axis, 'unknown(0x%02x)' % axis)
            self.axis_map.append(axis_name)
            self.axis_states[axis_name] = 0.0

        # Get the button map.
        buf = array.array('H', [0] * 200)
        ioctl(self.jostick_dev, 0x80406a34, buf)  # JSIOCGBTNMAP

        for btn in buf[:num_buttons]:
            btn_name = self.button_names.get(btn, 'unknown(0x%03x)' % btn)
            self.button_map.append(btn_name)
            self.button_states[btn_name] = 0

    def setup(self):
        evbuf = self.jostick_dev.read(8)
        if evbuf:
            time, value, type, number = struct.unpack('IhBB', evbuf)
            self.important_dict =  {
                "time": time,
                'value': value,
                'type' : type,
                'number' : number
            }

            if type & 0x01:
                return "button"

            if type & 0x02:
                return "joystick"

    def get_button(self):

        if self.important_dict['type'] & 0x01:
            button = self.button_map[self.important_dict['number']]
            if button:
                self.button_states[button] = self.important_dict['value']
                if self.important_dict['value']:
                    return "%s pressed" % (button)
                else:
                    return "%s relaesed" % (button)

    def get_joystick(self):
        axis = self.axis_map[self.important_dict['number']]
        if axis:
            fvalue = self.important_dict['value'] / 32767.0
            self.axis_states[axis] = fvalue
            return "%s:%.3f" % (axis, fvalue)


# if __name__ == '__main__':
#     myjoy = Joystic('/dev/input/js0')
#     while True:
#         if myjoy.setup() == "button":
#             button = myjoy.get_button()
#             print("======================================\n",
#                   "- button value =>", button)
#
#         if myjoy.setup() == "joystick":
#             joytick = myjoy.get_joystick()
#             print("======================================\n",
#                   "- joystick values =>", joytick)
