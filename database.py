import sqlite3


class Database:
    def __init__(self, database_file):
        self.cur = sqlite3.connect(database_file).cursor()

    def select(self, table_name, *columns):
        result_list = []
        columns_string = ", ".join(list(columns))
        res = self.cur.execute("SELECT {} FROM {}".format(columns_string, table_name))
        for row in res:
            result_list.append(row)
        return result_list

    def where(self, table, column_where, operator, value, *columns):
        result_list = []
        columns_string = ", ".join(list(columns))
        res = self.cur.execute("SELECT {} FROM {} WHERE {} {} {}".format(columns_string, table, column_where, operator, value))
        for row in res:
            result_list.append(row)
        return result_list

# if __name__ == '__main__':
#     db = Database("database.db")
#     colums = db.select('Characters', 'id', 'name')
#     print(colums)
