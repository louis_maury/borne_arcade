"""
Platformer Game
"""
import arcade
import mysql.connector

# db = mysql.connector.connect(host="localhost",user="root",password="sys",database="borne_arcade")
db = mysql.connector.connect(host="localhost",user="arcade",password="arcade",database="borne_arcade")
cursor = db.cursor()
# cursor.execute("""
#                CREATE TABLE IF NOT EXISTS personnages (
#                     id int(5) NOT NULL AUTO_INCREMENT,
#                     name varchar(50) DEFAULT NULL,
#                     life int(5) DEFAULT NULL,
#                     movement_speed int(5) DEFAULT NULL,
#                     jump_speed int(5) DEFAULT NULL,
#                     puissance int(5) DEFAULT NULL,
#                     PRIMARY KEY(id)
#                )
#                """)
#
# personnages = [("Barlog", "100", "3", "15", "20"),
#                ("Chun-Li", "70", "5", "17", "15")
#                ]
#
# insert_queries = """
#                INSERT INTO personnages (name, life, movement_speed, jump_speed, puissance)
#                VALUES(%s, %s, %s, %s, %s)
#                """

# cursor.executemany(insert_queries, personnages)
# db.commit()
cursor.execute("""
               SELECT * FROM personnages WHERE id=1
               """)

data_one = cursor.fetchone()
print(data_one)

cursor.execute("""
               SELECT * FROM personnages WHERE id=2
               """)

data_two = cursor.fetchone()
print(data_two)

db.close()

NUMBER_OF_COINS = 1

# Constantes de la fenêtre
SCREEN_WIDTH = 1920
SCREEN_HEIGHT = 1080
SCREEN_TITLE = "Ynov bagarre"

# Constantes utilisées pour donner une échelle aux sprites par rapport à leur taille réelle
# Sprite joueur
CHARACTER_SCALING = 1
# Sprite sol
TILE_SCALING = 2
# Nom du personnage
CHARACTER_ONE_NAME = data_one[1]
CHARACTER_TWO_NAME = data_two[1]
# Vitesse de déplacement du joueur, pixels par frame
CHARACTER_ONE_MOVEMENT_SPEED = data_one[3]
CHARACTER_TWO_MOVEMENT_SPEED = data_two[3]
# Vie du personnage
CHARACTER_ONE_LIFE = data_one[2]
CHARACTER_TWO_LIFE = data_two[2]
# Définition de la gravité (pour les sauts)
GRAVITY = 1
# Vitesse du saut
CHARACTER_ONE_JUMP_SPEED = data_one[4]
CHARACTER_TWO_JUMP_SPEED = data_two[4]
# Puissance du personnage
CHARACTER_ONE_POWER = data_one[5]
CHARACTER_TWO_POWER = data_two[5]
# Dépassement autorisé de la fenêtre
OFFSCREEN_SPACE = 0
# Délimitation de la fenêtre
LEFT_LIMIT = -OFFSCREEN_SPACE
RIGHT_LIMIT = SCREEN_WIDTH - OFFSCREEN_SPACE
BOTTOM_LIMIT = -OFFSCREEN_SPACE
TOP_LIMIT = SCREEN_HEIGHT - OFFSCREEN_SPACE
# Constantes utilisées pour déterminer le sens du sprite du personnage
TEXTURE_RIGHT = 1
TEXTURE_LEFT = 0
# Compteur
TICKS_PER_COUNTDOWN = 50
COUNTDOWN_FROM = 5

# Pour la police
DEFAULT_FONT_NAME = (
    "PRESS",
    "./resources/PressStart2P.ttf"
)