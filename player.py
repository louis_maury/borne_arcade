"""
Arcade Game
"""
import arcade
import os
from _variables import *
from open_window import *


class Player(arcade.Sprite):
    walk_sprite = None

    def __init__(self):
        super().__init__()

        self.physics_engine = None
        self.textures_p = arcade.SpriteList()
        # Load a left facing texture and a right facing texture.
        # mirrored=True will mirror the image we load.
        texture_p = arcade.Sprite("resources/" + CHARACTER_NAME + "/character.png", CHARACTER_SCALING)
        self.textures_p.append(texture_p)
        texture_p = arcade.Sprite("resources/" + CHARACTER_NAME + "/character.png", CHARACTER_SCALING)
        self.textures_p.append(texture_p)

        self.scale = CHARACTER_SCALING

        # By default, face right.
        # self.set_texture(TEXTURE_RIGHT)

    def setup(self):
        self.walk_list = arcade.SpriteList()
        self.walk_sprite = arcade.AnimatedWalkingSprite()

        # Position stand qui doit être obligatoirement défini
        self.walk_sprite.stand_right_textures = []
        self.walk_sprite.stand_right_textures.append(
            arcade.load_texture("resources/" + CHARACTER_NAME + "/walk0.png", scale=CHARACTER_SCALING))

        self.walk_sprite.stand_left_textures = []
        self.walk_sprite.stand_left_textures.append(
            arcade.load_texture("resources/" + CHARACTER_NAME + "/walk0.png", mirrored=True,
                                scale=CHARACTER_SCALING))

        # Sprites de déplacements
        self.walk_sprite.walk_right_textures = []
        for i in range(4):
            self.walk_sprite.walk_right_textures.append(
                arcade.load_texture("resources/" + CHARACTER_NAME + f"/barlog_walk{i}.png", scale=CHARACTER_SCALING))

        self.walk_sprite.walk_left_textures = []
        for i in range(4):
            self.walk_sprite.walk_left_textures.append(
                arcade.load_texture("resources/" + CHARACTER_NAME + f"/barlog_walk{i}.png", mirrored=True,
                                    scale=CHARACTER_SCALING))

        # Animation Sprite
        self.walk_sprite.center_x = SCREEN_WIDTH / 2
        self.walk_sprite.center_y = SCREEN_HEIGHT / 2

    def on_draw(self):
        """ Render the screen. """
        arcade.start_render()

        # Dessine les sprites
        self.textures_p.draw()
        self.walk_list.draw()

    def on_key_press(self, key, modifiers):
        """Called whenever a key is pressed. """
        # Définition des touches à utiliser
        if key == arcade.key.UP:
            if self.physics_engine.can_jump():
                self.walk_sprite.change_y = CHARACTER_JUMP_SPEED
                arcade.play_sound(self.jump_sound)
        # elif key == arcade.key.DOWN:
        #     self.player_sprite.change_y = -PLAYER_MOVEMENT_SPEED
        elif key == arcade.key.LEFT:
            self.walk_sprite.change_x = -CHARACTER_MOVEMENT_SPEED
        elif key == arcade.key.RIGHT:
            self.walk_sprite.change_x = CHARACTER_MOVEMENT_SPEED

    def on_key_release(self, key, modifiers):
        """Called when the user releases a key. """
        if key == arcade.key.LEFT:
            self.walk_sprite.change_x = 0
        elif key == arcade.key.RIGHT:
            self.walk_sprite.change_x = 0

    def on_update(self, delta_time):
        """ Movement and game logic """
        self.walk_list.update()
        self.walk_list.update_animation()
        # self.jump_sprite.update()

        # Move the player with the physics engine
        self.physics_engine.update()
        if self.walk_sprite.center_x <= LEFT_LIMIT:
            self.walk_sprite.center_x = RIGHT_LIMIT
        # if self.jump_sprite.center_x <= LEFT_LIMIT:
        #     self.jump_sprite.center_x = RIGHT_LIMIT
        if self.walk_sprite.center_x > RIGHT_LIMIT:
            self.walk_sprite.center_x = LEFT_LIMIT
        # if self.jump_sprite.center_x > RIGHT_LIMIT:
        #     self.jump_sprite.center_x = LEFT_LIMIT
        if self.walk_sprite.center_y > TOP_LIMIT:
            self.walk_sprite.center_y = BOTTOM_LIMIT
        if self.walk_sprite.center_y < BOTTOM_LIMIT:
            self.walk_sprite.center_y = TOP_LIMIT


def update(self):
    self.center_x += self.change_x
    self.center_y += self.change_y

    # Figure out if we should face left or right
    if self.change_x < 0:
        self.texture = self.textures[TEXTURE_LEFT]
    elif self.change_x > 0:
        self.texture = self.textures[TEXTURE_RIGHT]

    if self.left < 0:
        self.left = 0
    elif self.right > SCREEN_WIDTH - 1:
        self.right = SCREEN_WIDTH - 1

    if self.bottom < 0:
        self.bottom = 0
    elif self.top > SCREEN_HEIGHT - 1:
        self.top = SCREEN_HEIGHT - 1


def main():
    player = Player()
    player.setup()
