import database, os, arcade, _variables
import Joystick as JoyS

movement_speed = 5
dead_zone = 0.05


class Cursor(arcade.Sprite):
    def __init__(self, filename, joystick_number, center_x, center_y):
        super(Cursor, self).__init__(filename)
        joysticks = arcade.get_joysticks()
        if joysticks:
            self.joystick = joysticks[joystick_number]
            self.joystick.open()
        else:
            print("No joystick_number :'(")
            self.joystick = None

        self.center_x = center_x
        self.center_y = center_y

    def update(self):
        if self.joystick:
            self.change_x = self.joystick.x * movement_speed
            if abs(self.change_x) < dead_zone:
                self.change_x = 0

            self.change_y = -self.joystick.y * movement_speed
            if abs(self.change_y) < dead_zone:
                self.change_y = 0

        self.center_x += self.change_x
        self.center_y += self.change_y

        if self.left < 0:
            self.left = 0
        elif self.right > _variables.SCREEN_WIDTH - 1:
            self.right = _variables.SCREEN_WIDTH - 1

        if self.bottom < 0:
            self.bottom = 0
        elif self.top > _variables.SCREEN_HEIGHT - 1:
            self.top = _variables.SCREEN_HEIGHT - 1


class Grid:
    def __init__(self, x_start, y_start, nb_elements, box_width, box_height, content):
        self.x_start = x_start
        self.y_start = y_start
        self.nb_elements = nb_elements
        self.box_width = box_width
        self.box_height = box_height
        self.content = content

    def setup(self):
        liste = []
        scale = _variables.CHARACTER_SCALING
        for j in self.content:
            texture = arcade.load_texture(j)
            liste.append(texture)

        for i in range(self.nb_elements):
            x = self.x_start + (self.box_width + 5) * i
            arcade.draw_scaled_texture_rectangle(x, self.y_start, liste[i], scale, 0)


class ChooseCharacter(arcade.View):
    def __init__(self, screen_whidth, screen_height):
        file_path = os.path.dirname(os.path.abspath(__file__))
        os.chdir(file_path)
        super().__init__()
        arcade.set_viewport(0, screen_whidth - 1, 0, screen_height - 1)
        self.screen_width = screen_whidth
        self.scree_height = screen_height
        db = database.Database("database.db")
        self.characters_list = []
        temp_list = db.select('Characters', 'name')
        for element in temp_list:
            self.characters_list.append(element[0])

    def setup_1(self):
        self.p1_cursor_list = arcade.SpriteList()
        self.p1_cursor = Cursor("./resources/choose_character/p1_cursor.png", 0, 100, 100)
        self.button = JoyS.Joystic('/dev/input/js0')

    # def setup_2(self):
    #     self.p2_cursor_list = arcade.SpriteList()
    #     self.p2_cursor = Cursor("./resources/choose_character/p2_cursor.png", 1, 500, 500)
    #     self.p2_cursor_list.append(self.p2_cursor)

    def get_characters_list(self):
        liste = []
        for file in os.listdir("./resources/choose_character/"):
            for i in self.characters_list:
                if file.startswith(i):
                    liste.append("./resources/choose_character/{}".format(file))
        return liste

    def on_show(self):
        arcade.set_background_color(arcade.color.AFRICAN_VIOLET)
        self.char_list = None
        self.char_sprite = None
        self.char_list = arcade.SpriteList()

    def on_draw(self):
        image_source = self.get_characters_list()
        arcade.start_render()
        arcade.draw_text("Choose your character", self.screen_width / 2, self.scree_height - 50, arcade.color.WHITE,
                         font_size=20, anchor_x="center", font_name=_variables.DEFAULT_FONT_NAME)
        dat_grid = Grid(71 + self.screen_width / 3, self.scree_height / 2, 4, 142, 200, image_source)
        dat_grid.setup()
        self.char_list.draw()

        self.p1_cursor.draw()
        # self.p2_cursor.draw()

    def on_update(self, delta_time):
        self.p1_cursor.update()
        if self.button.setup() == "button":
            button = self.button.get_button()
            print(button)
        # self.p2_cursor.update()


if __name__ == '__main__':
    window = arcade.Window(_variables.SCREEN_WIDTH, _variables.SCREEN_HEIGHT, 'choose_character')
    choose_chara_view = ChooseCharacter(_variables.SCREEN_WIDTH, _variables.SCREEN_HEIGHT)
    window.show_view(choose_chara_view)
    choose_chara_view.setup_1()
    # choose_chara_view.setup_2()
    arcade.run()
