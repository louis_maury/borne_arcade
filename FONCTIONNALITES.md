# Borne arcade - Documentation fonctionnalités.

## Sommaire
- [Base de données](#base-de-donnees-file-cabinet)
- [Menus et interfaces](#menus-et-interfaces-three_button_mouse)
- [Gameplay](#gameplay-video_game)

## Base de données :file_cabinet: 
Pour pouvoir utiliser votre base de données, il vous faudra:
Un fichier sqlite (que nous nommerons ici `database.db`)

Un fois cela fait il vous suffit d'importer le fichier [`database.py`](database.py) dans votre programme:
```python
import database
```

### La commande `SELECT`
Dans votre programme, vous souhaitez executer la commande SQL:
```sql
SELECT id, name FROM Users;
```
Il vous suffit de faire:
```python
db = database.Database('database.db')
users_names = db.select('Users', 'id', 'name')
```
`users_names` sera une liste de tuples qui contiendront le contenu de vos selections

### La commande `WHERE`
> C'est bien beau de pouvoir faire une selection mais si il était possible de faire une selection avec la commande `WHERE` ce serait encore mieux !

Oui et c'est possible. Si vous souhaitez executer la commande suivante:
```sql
SELECT id, name, age
FROM Users
WHERE age < 20;
```
Il vous suffirat de faire ainsi:
```python
db = database.Database('database.db')
users_names = db.where('Users', 'age', '<', 20, 'id, name','age')
```

### Fonctions et parametres:
- `select(table, colonnes)`
    - table: Le nom de la table
    - colonnes: Les colonnes que vous souhaitez selectionner
- `where(table, colonne_du_where, oprérateur, valeur, colonnes)`
    - table: Le nom de la table
    - colonne_du_where: Colonne qui sera utiliser pour la fonction `WHERE`
    - opérateur: L'opérateur pour la fonction `WHERE`
    - valeur: La valeur qui sera utilisé pour la comparaison de la fonction `WHERE`
    - colonnes: Les colonnes que vous souhaitez selectionner

**PRO TIPS:**
- Vous pouvez selectionner autant de colones que vous le souhaitez
- Si vous voulez selectionner toutes les colonnes vous pouvez mettre `'*'` en parametre à la place du nom de toutes les colonnes.

## Menus et interfaces :three_button_mouse:
Plusieurs interfaces différentes sont accessible dans le jeux, toutes mènent à différents menus ou au jeu en lui-même.

Le point d'entrée de l'application est le fichier [menu.py](/menu.py) dans lequel toutes les interfaces sont créées.

La première à laquelle nous accédons est `MenuView`. Nous initialisons cette vue un background. Puis nous *dessinons* des textes (*draw_text*) pour donner des informations à l'utilisateur. Ces indications sont rattachées à des actions de touches à utiliser. Par exemple, en pressant la touche `O` l'utilisateur accèdera à l'interface des commandes du jeu l'`InstructionsMenu`. Qui fonctionne comme les autres, depuis cette interface, nous pouvons retourner en arrière.

Le `main()` de cette classe instanciera une fenêtre d'application d'une taille donnée, dans laquelle toutes les vues créées s'intègreront.

Il existe également un menu `Pause`, qui mettra en pause toutes les sprites durant le combat en pressant la touche *Echap*. Il laissera visible les sprites des joueurs afin de savoir comment reprendre son combat, recommencer ou quitter le jeu.

## Gameplay :video_game:
Certaines données du projet sont stockées dans des variables globales que l'on peut retrouver dans le fichier [_variables.py](ressources/_variables.py).

L'ensemble du gameplay se trouve dans [open_window.py](/open_window.py). Dans ce fichier, les sprites et les fonctionnalités sont instanciés.

On commence par initialiser l'ensemble des informations, sprites et listes d'animations dans `__init__`. Tout ceci étant prêt, nous appelons la prochaine fonction `setup()` qui permettra d'initialiser le jeu et de remplir les listes de sprites afin d'effectuer des animations.

Dans cette fonction `setup()` nous dessinons également un *sol* pour définir une base pour positionner les personnages et gérer la gravité. Grâce à une sprite fournie par la librairie, nous bouclons sur ce sprite en l'ajoutant à chaque fois dans une liste, pour que le sol se dessine tout en long de la fenêtre. Ce sol que nous mettrons transparent pour ne pas gâcher la carte.
Une fontion `objects()` permet elle de faire apparaitre tous les *X* temps (grâce à *Timer* fournit par python) un objet bonus sur la carte que les joueurs peuvent récupérer pour tenter de gagner le combat.

Ici, la fonction `on_draw()` permet également de dessiner les sprites défini plus haut. On retrouve aussi d'abord, un condition `if` qui permet de gérer une compteur avant de débuter le combat. Après avoir défini le temps entre chaque secondes, on *draw* chaque secondes puis arriver à 0, un sprite fight s'affiche et le combat peut commencer.
On dessine ici aussi, les informations sur les joueurs choisis, à savoir leur nom ainsi que leur barre de vie.

Pour rendre tout ça dynamique, nous définissons des touches à presser pour intéragir avec les sprites dans la fonction `on_key_press()`. Ici, chaque touche est mappée pour les différents joueurs. Par exemple, le *Joueur 1* se déplacera avec `Z`, `Q`, `D` tandis que le *Joueur 2* utilisera les flêches directionnelles.

La fonction `on_key_release()` fera l'inverse, dès qu'une touche de déplacement est relacher, l'axe utilisé se remet à 0 afin que le personnage ne se déplace plus.

La dernière fonction `on_update()` permet justement de mettre à jour l'état de sprite jusqu'au prochain qu'il trouve dans sa liste associée, de boucler jusqu'à la fin, ou d'en repartir du début. Ici, on tente également de limiter les joueurs à sortir de la fenêtre. On retrouvera ici la gestion des *hitbox*. Celles des personnages ainsi que des objets à récupérer. On gère par conséquent, le fait qu'une *hitbox* en touche une autre et d'appliquer les modifications en conséquences (perte de vie, gain bonus, etc.).

C'est ici que l'on retrouvera la création du menu `Pause`.

**Good Game & Have Fun**
