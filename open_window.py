"""
Platformer Game
"""
from threading import Timer

import arcade
import random
from _variables import *


class MyGame(arcade.View):
    """
    Main application class.
    """
    def __init__(self):
        # Call the parent class and set up the window
        super().__init__()

        # Countdown
        #Sprite
        self.fight_list = arcade.SpriteList()
        self.fight_sprite = arcade.Sprite("resources/fight.png")
        self.fight_sprite.center_x = SCREEN_WIDTH / 2
        self.fight_sprite.center_y = SCREEN_HEIGHT / 2
        self.fight_list.append(self.fight_sprite)
        self.coin_list = arcade.SpriteList()
        # Values
        self.countdown_counter = (COUNTDOWN_FROM + 1) * TICKS_PER_COUNTDOWN
        self.countdown_values = range(1, COUNTDOWN_FROM + 1)

        # Character one infos
        self.char_one_name_top = 950
        self.char_one_name_left = 100
        self.char_one_life_top = 930
        self.char_one_life_left = 370

        ### VS ###
        self.vs_list = arcade.SpriteList()
        self.vs_sprite = arcade.Sprite("resources/vs.bmp")
        self.vs_sprite.center_x = SCREEN_WIDTH / 2
        self.vs_sprite.center_y = SCREEN_HEIGHT / 2 + 400
        self.vs_list.append(self.vs_sprite)

        # Character two infos
        self.char_two_name_top = 950
        self.char_two_name_left = 1460
        self.char_two_life_top = 930
        self.char_two_life_left = 1540

        # Variables that will hold sprite lists
        ### TMP ###
        self.view_bottom = 20
        self.view_left = 20
        ### TMP ###
        self.floor_list = arcade.SpriteList()
        self.move_one_sprites_list = arcade.SpriteList()
        self.move_two_sprites_list = arcade.SpriteList()
        self.coin = arcade.AnimatedWalkingSprite(CHARACTER_SCALING)

        # Set up the player info
        self.player_one_sprite = arcade.AnimatedWalkingSprite(CHARACTER_SCALING)
        self.player_two_sprite = arcade.AnimatedWalkingSprite(CHARACTER_SCALING)
        self.jump_sound = arcade.load_sound("resources/jump.wav")

        # Variable physique/gravité
        self.physics_one_engine = None
        self.physics_two_engine = None

        # Initilisation du score
        self.score = 0

        # Set up du background
        self.background = arcade.load_texture("resources/background.gif")

        self.setup()

    def setup(self):
        """ Set up the game here. Call this function to restart the game. """
        # Animation Sprite
        self.player_one_sprite.center_x = SCREEN_WIDTH / 2 - 600
        self.player_two_sprite.center_x = SCREEN_WIDTH / 2 + 600
        self.player_one_sprite.center_y = SCREEN_HEIGHT
        self.player_two_sprite.center_y = SCREEN_HEIGHT

        # Position stand qui doit être obligatoirement défini
        self.player_one_sprite.stand_right_textures = []
        self.player_one_sprite.stand_right_textures.append(arcade.load_texture("resources/" + CHARACTER_ONE_NAME + "/character.png"))
        self.player_two_sprite.stand_right_textures = []
        self.player_two_sprite.stand_right_textures.append(arcade.load_texture("resources/" + CHARACTER_TWO_NAME + "/character.png"))

        self.player_one_sprite.stand_left_textures = []
        self.player_one_sprite.stand_left_textures.append(arcade.load_texture("resources/" + CHARACTER_ONE_NAME + "/character.png", mirrored=True))
        self.player_two_sprite.stand_left_textures = []
        self.player_two_sprite.stand_left_textures.append(arcade.load_texture("resources/" + CHARACTER_TWO_NAME + "/character.png", mirrored=True))

        # Sprites de déplacements
        self.player_one_sprite.walk_right_textures = []
        for i in range(0, 4):
            self.player_one_sprite.walk_right_textures.append(arcade.load_texture("resources/" + CHARACTER_ONE_NAME + f"/walk{i}.png"))
        self.player_two_sprite.walk_right_textures = []
        for i in range(0, 7):
            self.player_two_sprite.walk_right_textures.append(arcade.load_texture("resources/" + CHARACTER_TWO_NAME + f"/walk{i}.png"))

        self.player_one_sprite.walk_left_textures = []
        for i in range(0, 3):
            self.player_one_sprite.walk_left_textures.append(arcade.load_texture("resources/" + CHARACTER_ONE_NAME + f"/walk{i}.png", mirrored=True))
        self.player_two_sprite.walk_left_textures = []
        for i in range(0, 7):
            self.player_two_sprite.walk_left_textures.append(arcade.load_texture("resources/" + CHARACTER_TWO_NAME + f"/walk{i}.png", mirrored=True))

        self.move_one_sprites_list.append(self.player_one_sprite)
        self.move_two_sprites_list.append(self.player_two_sprite)

        # Définition du score
        self.score = 0

        # Set up du sol
        for x in range(0, 1920, 70):
            floor = arcade.Sprite("resources/grassMid.png", TILE_SCALING)
            floor.center_x = x
            floor.center_y = 0
            # Set sol transparent. 0 to 255
            floor.alpha = 0
            self.floor_list.append(floor)

        def objects():
            # Create ojects
            for i in range(NUMBER_OF_COINS):
                self.coin = arcade.Sprite(":resources:images/items/coinGold.png")
                coin_placed = False
                while not coin_placed:
                    self.coin.center_x = random.randrange(SCREEN_WIDTH)
                    self.coin.center_y = random.randrange((SCREEN_HEIGHT / 2) - TILE_SCALING)

                    coin_list_hit = arcade.check_for_collision_with_list(self.coin, self.coin_list)

                    if len(coin_list_hit) == 0:
                        coin_placed = True
                        time = random.randint(1, 4)
                        t = Timer(time, objects)
                        t.start()
                self.coin_list.append(self.coin)

        time = random.randint(2, 4)
        t = Timer(time, objects)
        t.start()

        # Mise en place de la physique grâce à la gravité
        self.physics_one_engine = arcade.PhysicsEnginePlatformer(self.player_one_sprite, self.floor_list, GRAVITY)
        self.physics_two_engine = arcade.PhysicsEnginePlatformer(self.player_two_sprite, self.floor_list, GRAVITY)

    def on_draw(self):
        """ Render the screen. """
        arcade.start_render()
        # Dessine Background image
        arcade.draw_texture_rectangle(SCREEN_WIDTH // 2, SCREEN_HEIGHT // 2,
                                      SCREEN_WIDTH, SCREEN_HEIGHT, self.background)
        # Dessine les sprites
        self.move_one_sprites_list.draw()
        self.move_two_sprites_list.draw()
        self.floor_list.draw()
        self.coin_list.draw()

        # Dessine le score TMP
        score_text = f"Score: {self.score}"
        arcade.draw_text(score_text, 10 + self.view_left, 10 + self.view_bottom,
                         arcade.csscolor.WHITE, 18, font_name=DEFAULT_FONT_NAME)

        # Countdown timer
        if self.countdown_counter > TICKS_PER_COUNTDOWN:
            arcade.draw_text(f"{self.countdown_values[(self.countdown_counter - TICKS_PER_COUNTDOWN - 1) // TICKS_PER_COUNTDOWN]}", SCREEN_WIDTH / 2, SCREEN_HEIGHT / 2, arcade.color.RED, font_size=80, anchor_x="center", font_name=DEFAULT_FONT_NAME)
        elif self.countdown_counter:
            self.fight_list.draw()

        # Character one infos
        name_one = f"{CHARACTER_ONE_NAME}"
        arcade.draw_text(name_one.upper(), 10 + self.char_one_name_left, 10 + self.char_one_name_top,
                         arcade.csscolor.WHITE, 40, font_name=DEFAULT_FONT_NAME)
        arcade.draw_rectangle_filled(self.char_one_life_left, self.char_one_life_top, 550, 20, arcade.color.GREEN)

        ### VS ###
        self.vs_list.draw()
        ### VS ###

        # Character two infos
        name_two = f"{CHARACTER_TWO_NAME}"
        arcade.draw_text(name_two.upper(), self.char_two_name_left, 10 + self.char_two_name_top,
                         arcade.csscolor.WHITE, 40, font_name=DEFAULT_FONT_NAME)
        arcade.draw_rectangle_filled(self.char_two_life_left, self.char_two_life_top, 550, 20, arcade.color.GREEN)

    def on_key_press(self, key, modifiers):
        """Called whenever a key is pressed. """
        # Définition des touches à utiliser
        if key == arcade.key.Z:
            if self.physics_one_engine.can_jump():
                self.player_one_sprite.change_y = CHARACTER_ONE_JUMP_SPEED
                self.score += 1
                arcade.play_sound(self.jump_sound)
        # elif key == arcade.key.DOWN:
        #     self.player_sprite.change_y = -PLAYER_MOVEMENT_SPEED
        elif key == arcade.key.Q:
            self.player_one_sprite.change_x = -CHARACTER_ONE_MOVEMENT_SPEED
        elif key == arcade.key.D:
            self.player_one_sprite.change_x = CHARACTER_ONE_MOVEMENT_SPEED
        # elif key == arcade.key.G:
        #     self.player_one_punch_sprite = CHARACTER_ONE_MOVEMENT_SPEED

        if key == arcade.key.UP:
            if self.physics_two_engine.can_jump():
                self.player_two_sprite.change_y = CHARACTER_TWO_JUMP_SPEED
        elif key == arcade.key.LEFT:
            self.player_two_sprite.change_x = -CHARACTER_TWO_MOVEMENT_SPEED
        elif key == arcade.key.RIGHT:
            self.player_two_sprite.change_x = CHARACTER_TWO_MOVEMENT_SPEED

        if key == arcade.key.ESCAPE:
            pause = PauseView(self)
            self.window.show_view(pause)

    def on_key_release(self, key, modifiers):
        """Called when the user releases a key. """
        if key == arcade.key.Q:
            self.player_one_sprite.change_x = 0
        elif key == arcade.key.D:
            self.player_one_sprite.change_x = 0
        elif key == arcade.key.LEFT:
            self.player_two_sprite.change_x = 0
        elif key == arcade.key.RIGHT:
            self.player_two_sprite.change_x = 0

    def on_update(self, delta_time, CHARACTER_ONE_JUMP_SPEED=CHARACTER_ONE_JUMP_SPEED):
        """ Movement and game logic """
        # self.jump_one_sprites_list.update()
        self.move_one_sprites_list.update()
        self.move_two_sprites_list.update()
        # self.jump_one_sprites_list.update_animation()
        self.move_one_sprites_list.update_animation()
        self.move_two_sprites_list.update_animation()

        # Move the player with the physics engine
        self.physics_one_engine.update()
        if self.player_one_sprite.center_x <= LEFT_LIMIT:
            self.player_one_sprite.center_x = RIGHT_LIMIT
        if self.player_one_sprite.center_x > RIGHT_LIMIT:
            self.player_one_sprite.center_x = LEFT_LIMIT
        if self.player_one_sprite.center_y > TOP_LIMIT:
            self.player_one_sprite.center_y = BOTTOM_LIMIT
        if self.player_one_sprite.center_y < BOTTOM_LIMIT:
            self.player_one_sprite.center_y = TOP_LIMIT

        # Move the player with the physics engine
        self.physics_two_engine.update()
        if self.player_two_sprite.center_x <= LEFT_LIMIT:
            self.player_two_sprite.center_x = RIGHT_LIMIT
        if self.player_two_sprite.center_x > RIGHT_LIMIT:
            self.player_two_sprite.center_x = LEFT_LIMIT
        if self.player_two_sprite.center_y > TOP_LIMIT:
            self.player_two_sprite.center_y = BOTTOM_LIMIT
        if self.player_two_sprite.center_y < BOTTOM_LIMIT:
            self.player_two_sprite.center_y = TOP_LIMIT

        player_hits = arcade.check_for_collision(self.player_one_sprite, self.player_two_sprite)
        if player_hits:
            # self.player_one_sprite.remove_from_sprite_lists()
            self.score += 1
            
        player_one_coin_hits = arcade.check_for_collision_with_list(self.player_one_sprite, self.coin_list)
        player_two_coin_hits = arcade.check_for_collision_with_list(self.player_two_sprite, self.coin_list)
        for hit in player_one_coin_hits:
            self.score += 1
            CHARACTER_ONE_JUMP_SPEED + 10
            hit.remove_from_sprite_lists()
            return CHARACTER_ONE_JUMP_SPEED
        for hit in player_two_coin_hits:
            self.score += 1
            CHARACTER_ONE_JUMP_SPEED += 10
            hit.remove_from_sprite_lists()

        # Décrémentation countdown timer
        if self.countdown_counter:
            self.countdown_counter -= 1
            # Sprite no move
            # if self.countdown_counter == TICKS_PER_COUNTDOWN:
                # self.player_one_sprite.state = (self.player_one_sprite.stand_right_textures, )
                # self.player_two_sprite.state = (self.player_two_sprite.stand_left_textures, )


class PauseView(arcade.View):
    def __init__(self, game_view):
        super().__init__()
        self.game_view = game_view

    def on_show(self):
        arcade.set_background_color(arcade.color.ORANGE)

    def on_draw(self):
        arcade.start_render()

        # Draw player, for effect, on pause screen.
        # The previous View (GameView) was passed in
        # and saved in self.game_view.
        player_one_sprite = self.game_view.player_one_sprite
        player_one_sprite.draw()
        player_two_sprite = self.game_view.player_two_sprite
        player_two_sprite.draw()

        # draw an orange filter over him
        arcade.draw_lrtb_rectangle_filled(left=player_one_sprite.left,
                                          right=player_one_sprite.right,
                                          top=player_one_sprite.top,
                                          bottom=player_one_sprite.bottom,
                                          color=arcade.color.ORANGE + (200,))
        arcade.draw_lrtb_rectangle_filled(left=player_two_sprite.left,
                                          right=player_two_sprite.right,
                                          top=player_two_sprite.top,
                                          bottom=player_two_sprite.bottom,
                                          color=arcade.color.ORANGE + (200,))

        arcade.draw_text("PAUSED", SCREEN_WIDTH/2, SCREEN_HEIGHT/2+50,
                         arcade.color.BLACK, font_size=50, anchor_x="center", font_name=DEFAULT_FONT_NAME)

        # Show tip to return or reset
        arcade.draw_text("PRESS ESCAPE TO RETURN",
                         SCREEN_WIDTH/2,
                         SCREEN_HEIGHT/2,
                         arcade.color.BLACK,
                         font_name=DEFAULT_FONT_NAME,
                         font_size=20,
                         anchor_x="center")
        arcade.draw_text("PRESS ENTER TO RESTART",
                         SCREEN_WIDTH/2,
                         SCREEN_HEIGHT/2-30,
                         arcade.color.BLACK,
                         font_size=20,
                         font_name=DEFAULT_FONT_NAME,
                         anchor_x="center")

    def on_key_press(self, key, _modifiers):
        if key == arcade.key.ESCAPE:   # resume game
            self.window.show_view(self.game_view)
        elif key == arcade.key.ENTER:  # reset game
            game = MyGame()
            self.window.show_view(game)
        elif key == arcade.key.Q:
            menu = menu.MenuView()
            self.window.show_view(menu)


def main():
    """ Main method """
    window = MyGame()
    window.setup()
    arcade.run()


if __name__ == "__main__":
    main()
