# BORNE ARCADE
## YNOV BAGARRE

[Trello](https://trello.com/b/W145xXnk)

### Sommaire
+ [Description du projet](#description-du-projet)
+ [Technologie utilisée](#technologie-utilise)
+ [Installation du projet](#installation-du-projet)
    + [Linux](#linux)

### Description du projet
Ynov Bagarre est un jeu de type arcade, typique des années 80 sur les très connues borne d'arcade massivement utilisées à l'époque.

Inspiré de la license Street Fighter, vous retrouvez quelques uns des combattants du jeu de combat le plus connu.
Depuis le menu principal, vous aurez accès au menu vous expliquant les touches à utiliser sur votre clavier. Il vous suffira d'appuyer sur la touche *Entrée* pour lancer une partie.

### Technologie utilisée
Le jeu est développé avec le language Python et la librairie [Arcade](https://arcade.academy/index.html).

Arcade est une librairie très complète permettant de créer des jeux vidéos en 2D grâce à la manipulation de *Sprites*.
Ne l'ayant jamais manipulée auparavant, nous découvrons la librairie tout en développant notre projet.

### Installation du projet
Voici les étapes à suivre pour pouvoir tester notre jeu :

+ Cloner le dépôt :
```shell script
git clone https://gitlab.com/louis_maury/borne_arcade
```

+ Accèder au dossier :
```shell script
cd borne_arcade
```

+ Installer les paquets nécessaires :
```shell script
pip install requirements.txt
```

+ Exécutez le launcher :
```shell script
python3 menu.py
```

[Documentation des fonctionnalités du projet](/FONCTIONNALITES.md)