import arcade
from _variables import *
from open_window import MyGame

COLORS_START = [arcade.color.BUD_GREEN, arcade.color.GREEN]


class MenuView(arcade.View):
    def __init__(self):
        super().__init__()
        # Set up du background
        self.background = arcade.load_texture("resources/menu.jpg")
        self.index = 0

    def on_draw(self):
        self.index += 1
        arcade.start_render()
        arcade.draw_texture_rectangle(SCREEN_WIDTH // 2, SCREEN_HEIGHT // 2, SCREEN_WIDTH, SCREEN_HEIGHT, self.background)
        color_start = COLORS_START[(self.index // 40) % 2]
        arcade.draw_text(
            "YNOV BAGARRE",
            SCREEN_WIDTH / 4,
            SCREEN_HEIGHT / 2 + 400,
            arcade.color.DARK_CHESTNUT,
            font_size=80,
            font_name=DEFAULT_FONT_NAME,
            anchor_x="center",
        )
        arcade.draw_text(
            "PRESS ENTER TO START",
            SCREEN_WIDTH / 5,
            SCREEN_HEIGHT / 2 - 60,
            color_start,
            font_size=40,
            font_name=DEFAULT_FONT_NAME,
            anchor_x="center",
        )
        arcade.draw_text(
            "PRESS O TO INSTRUCTIONS",
            SCREEN_WIDTH / 5,
            SCREEN_HEIGHT / 2 - 100,
            arcade.color.ORANGE,
            font_size=25,
            font_name=DEFAULT_FONT_NAME,
            anchor_x="center",
        )
        arcade.draw_text(
            "LOUIS MAURY & MAXIME LARRIEU",
            SCREEN_WIDTH / 2 + 750,
            SCREEN_HEIGHT / 15,
            arcade.color.DARK_LAVA,
            font_size=15,
            font_name=DEFAULT_FONT_NAME,
            anchor_x="center"
        )
        arcade.draw_text(
            "BORDEAUX YNOV CAMPUS",
            SCREEN_WIDTH / 2 + 750,
            SCREEN_HEIGHT / 20,
            arcade.color.DARK_LAVA,
            font_size=10,
            font_name=DEFAULT_FONT_NAME,
            anchor_x="center"
        )

    def on_key_press(self, key, modifiers):
        if key == arcade.key.ENTER:
            game_window = MyGame()
            self.window.show_view(game_window)
        elif key == arcade.key.O:
            instruction_view = InstructionsMenu()
            self.window.show_view(instruction_view)


class InstructionsMenu(arcade.View):
    def __init__(self):
        super().__init__()
        # Set up du background
        self.background = arcade.load_texture("resources/menu.jpg")

    def on_draw(self):
        arcade.start_render()
        arcade.draw_texture_rectangle(SCREEN_WIDTH // 2, SCREEN_HEIGHT // 2, SCREEN_WIDTH, SCREEN_HEIGHT, self.background)
        arcade.draw_text(
            "YNOV BAGARRE",
            SCREEN_WIDTH / 4,
            SCREEN_HEIGHT / 2 + 400,
            arcade.color.BLACK,
            font_size=80,
            font_name=DEFAULT_FONT_NAME,
            anchor_x="center",
        )
        arcade.draw_text(
            "INSTRUCTIONS",
            SCREEN_WIDTH / 4,
            SCREEN_HEIGHT / 2 + 300,
            arcade.color.RED_DEVIL,
            font_size=50,
            font_name=DEFAULT_FONT_NAME,
            anchor_x="center",
        )
        # PLAYER 1
        arcade.draw_text(
            "PLAYER 1",
            SCREEN_WIDTH / 9.5,
            SCREEN_HEIGHT / 2,
            arcade.color.GRAY_ASPARAGUS,
            font_size=35,
            font_name=DEFAULT_FONT_NAME,
            anchor_x="center"
        )
        arcade.draw_text(
            "USE Q AND D KEYS TO MOVE",
            SCREEN_WIDTH / 9.5,
            SCREEN_HEIGHT / 2 - 40,
            arcade.color.GRAY,
            font_size=15,
            font_name=DEFAULT_FONT_NAME,
            anchor_x="center",
        )
        arcade.draw_text(
            "USE Z KEY TO JUMP",
            SCREEN_WIDTH / 9.5,
            SCREEN_HEIGHT / 2 - 70,
            arcade.color.GRAY,
            font_size=15,
            font_name=DEFAULT_FONT_NAME,
            anchor_x="center",
        )
        arcade.draw_text(
            "USE S KEY TO SIT",
            SCREEN_WIDTH / 9.5,
            SCREEN_HEIGHT / 2 - 100,
            arcade.color.GRAY,
            font_size=15,
            font_name=DEFAULT_FONT_NAME,
            anchor_x="center",
        )
        # PLAYER 2
        arcade.draw_text(
            "PLAYER 2",
            SCREEN_WIDTH / 3.5,
            SCREEN_HEIGHT / 2,
            arcade.color.GRAY_ASPARAGUS,
            font_size=35,
            font_name=DEFAULT_FONT_NAME,
            anchor_x="center"
        )
        arcade.draw_text(
            "USE LEFT AND RIGHT ARROWS TO MOVE",
            SCREEN_WIDTH / 3.5,
            SCREEN_HEIGHT / 2 - 40,
            arcade.color.GRAY,
            font_size=15,
            font_name=DEFAULT_FONT_NAME,
            anchor_x="center",
        )
        arcade.draw_text(
            "USE UP ARROW TO JUMP",
            SCREEN_WIDTH / 3.5,
            SCREEN_HEIGHT / 2 - 70,
            arcade.color.GRAY,
            font_size=15,
            font_name=DEFAULT_FONT_NAME,
            anchor_x="center",
        )
        arcade.draw_text(
            "USE DOWN ARROW TO SIT",
            SCREEN_WIDTH / 3.5,
            SCREEN_HEIGHT / 2 - 100,
            arcade.color.GRAY,
            font_size=15,
            font_name=DEFAULT_FONT_NAME,
            anchor_x="center",
        )
        arcade.draw_text(
            "USE BACKSPACE TO RETURN AT MENU",
            SCREEN_WIDTH / 4,
            SCREEN_HEIGHT / 20,
            arcade.color.RED,
            font_size=20,
            font_name=DEFAULT_FONT_NAME,
            anchor_x="center"
        )
        arcade.draw_text(
            "LOUIS MAURY & MAXIME LARRIEU",
            SCREEN_WIDTH / 2 + 750,
            SCREEN_HEIGHT / 15,
            arcade.color.DARK_LAVA,
            font_size=15,
            font_name=DEFAULT_FONT_NAME,
            anchor_x="center"
        )
        arcade.draw_text(
            "BORDEAUX YNOV CAMPUS",
            SCREEN_WIDTH / 2 + 750,
            SCREEN_HEIGHT / 20,
            arcade.color.DARK_LAVA,
            font_size=10,
            font_name=DEFAULT_FONT_NAME,
            anchor_x="center"
        )

    def on_key_press(self, key, modifiers):
        if key == arcade.key.BACKSPACE:
            menu_view = MenuView()
            self.window.show_view(menu_view)


def main():
    window = arcade.Window(SCREEN_WIDTH, SCREEN_HEIGHT, SCREEN_TITLE)
    menu = MenuView()
    window.show_view(menu)
    arcade.run()


if __name__ == "__main__":
    main()
